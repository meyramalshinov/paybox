<?php

include("PG_Signature.php");
include_once("database.php");


$MERCHANT_SECRET_KEY = "cobitewodolejaxy";

$thisScriptName = PG_Signature::getOurScriptName();

if ( !PG_Signature::check($_GET['pg_sig'], $thisScriptName, $_GET, $MERCHANT_SECRET_KEY) )
    die("Bad signature");

$code = null;

$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$pg_salt = null;
$pg_order_id = null;
$pg_payment_id = null;
$pg_amount = null;
$pg_currency = null;
$pg_net_amount = null;
$pg_ps_amount = null;
$pg_ps_full_amount = null;
$pg_ps_currency = null;
$pg_payment_system = null;
$pg_result = null;
$pg_payment_date = null;
$pg_can_reject = null;
$pg_user_phone = null;
$pg_need_phone_notification = null;
$pg_card_brand = null;
$pg_card_pan = null;
$pg_card_hash = null;
$pg_captured = null;
$pg_sig = null;

$pg_salt = $_REQUEST['pg_salt'];
$pg_order_id = $_REQUEST['pg_order_id'];
$pg_payment_id = $_REQUEST['pg_payment_id'];
$pg_amount = $_REQUEST['pg_amount'];
$pg_currency = $_REQUEST['pg_currency'];
$pg_net_amount = $_REQUEST['pg_net_amount'];
$pg_ps_amount = $_REQUEST['pg_ps_amount'];
$pg_ps_full_amount = $_REQUEST['pg_ps_full_amount'];
$pg_ps_currency = $_REQUEST['pg_ps_currency'];
$pg_payment_system = $_REQUEST['pg_payment_system'];
$pg_result = $_REQUEST['pg_result'];
$pg_payment_date = $_REQUEST['pg_payment_date'];
$pg_can_reject = $_REQUEST['pg_can_reject'];
$pg_user_phone = $_REQUEST['pg_user_phone'];
$pg_need_phone_notification = $_REQUEST['pg_need_phone_notification'];
$pg_card_brand = $_REQUEST['pg_card_brand'];
$pg_card_pan = $_REQUEST['pg_card_pan'];
$pg_card_hash = $_REQUEST['pg_card_hash'];
$pg_captured = $_REQUEST['pg_captured'];
$pg_sig = $_REQUEST['pg_sig'];

if ( $_GET['pg_result'] == 1 ) {
	// обрабатываем случай успешной оплаты заказа с номером $order_id
	$code = 'OK';
}
else {
	// заказ с номером $order_id не будет оплачен.
	$code = 'ERROR';
}

$sql = 'UPDATE `orders` SET 
			`code` = ?,
			`pg_salt` = ?,
			`pg_order_id` = ?,
			`pg_payment_id` = ?,
			`pg_amount` = ?,
			`pg_currency` = ?,
			`pg_net_amount` = ?,
			`pg_ps_amount` = ?,
			`pg_ps_full_amount` = ?,
			`pg_ps_currency` = ?,
			`pg_payment_system` = ?,
			`pg_result` = ?,
			`pg_payment_date` = ?,
			`pg_can_reject` = ?,
			`pg_user_phone` = ?,
			`pg_need_phone_notification` = ?,
			`pg_card_brand` = ?,
			`pg_card_pan` = ?,
			`pg_card_hash` = ?,
			`pg_captured` = ?,
			`pg_sig` = ?
		WHERE `id` = ?';

$q = $pdo->prepare($sql);
$q->execute(array(
		$code,
		$pg_salt,
		$pg_order_id,
		$pg_payment_id,
		$pg_amount,
		$pg_currency,
		$pg_net_amount,
		$pg_ps_amount,
		$pg_ps_full_amount,
		$pg_ps_currency,
		$pg_payment_system,
		$pg_result,
		$pg_payment_date,
		$pg_can_reject,
		$pg_user_phone,
		$pg_need_phone_notification,
		$pg_card_brand,
		$pg_card_pan,
		$pg_card_hash,
		$pg_captured,
		$pg_sig,
		$pg_order_id
	));

Database::disconnect();

$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><response/>');
$xml->addChild('pg_salt', $pg_salt);
$xml->addChild('pg_status', 'ok');
$xml->addChild('pg_description', "Оплата принята");
$xml->addChild('pg_sig', PG_Signature::makeXML($thisScriptName, $xml, $MERCHANT_SECRET_KEY));

header('Content-type: text/xml');
print $xml->asXML();
?>