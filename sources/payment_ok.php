<?php
include("PG_Signature.php");

$MERCHANT_SECRET_KEY = "cobitewodolejaxy";


$arrParams = $_GET;
$thisScriptName = PG_Signature::getOurScriptName();

if ( !PG_Signature::check($arrParams['pg_sig'], $thisScriptName, $arrParams, $MERCHANT_SECRET_KEY) )
    die("Bad signature");

$order_id = $arrParams['pg_order_id'];
$payment_id= $arrParams['pg_payment_id'];

?>

<!DOCTYPE html>
<html lang="en" class="login_page">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Оплата произведена успешно!</title>
    
        <!-- Bootstrap framework -->
            <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
        <!-- theme color-->
            <link rel="stylesheet" href="css/blue.css" />
        <!-- tooltip -->    
			<link rel="stylesheet" href="lib/qtip2/jquery.qtip.min.css" />
        <!-- main styles -->
            <link rel="stylesheet" href="css/style.css" />
    
        <!-- favicon -->
            <link rel="shortcut icon" href="favicon.ico" />
    
        <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    
        <!--[if lt IE 9]>
            <script src="js/ie/html5.js"></script>
			<script src="js/ie/respond.min.js"></script>
        <![endif]-->
		
    </head>
    <body>
		
		<div class="login_box">
		
			
			<form method="post" id="login_form">
				<div class="top_b"><img src="logo-2.png" /></div>    
				<div class="cnt_b">
					<div style="float:right">
						ТОО "Data Discovery Services"<br />
						<a href="">http://datadiscovery.kz</a> <br />
						<a href="">http://moysklad.com.kz</a> <br />						
					</div>
					<div style="clear:both;"></div>
					<div class="form-group">
						<div style="font-weight: bold;margin:10px 0 10px 0;">Детали платежа:</div>
						<div>
							ID заказа: <span style="font-weight:bolder;"><?php echo $order_id; ?></span>
						</div>
						<div>
							Номер платежа: <span style="font-weight:bolder;"><?php echo $payment_id; ?></span>
						</div>
						<div>
							Статус платежа: <span style="color:green;font-weight:bolder;">платеж принят</span>
						</div>
						<div style="font-weight: bolder;margin: 20px 0 0 0;">
							Доступ будет предоставлен в течение 1 суток
						</div>
					</div>
				</div>
				<div class="btm_b clearfix">
					<a href="http://moysklad.com.kz/"><div class="btn btn-default btn-sm pull-right" >Вернуться в moysklad.com.kz</div></a>
				</div>  
			</form>
		
			<div class="links_b links_btm clearfix" style="font-size: 14px;color: red;">
				Спасибо за покупку!
			</div>
			
		</div>
    </body>
</html>
