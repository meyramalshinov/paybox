<?php 
	include_once 'database.php';
	$pdo = Database::connect();
?>

<html>
	<head>
		<style type="text/css">
			table {
				font-family: verdana,arial,sans-serif;
				font-size:8px;
				color:#333333;
				border-width: 1px;
				border-color: #999999;
				border-collapse: collapse;
			}
			table th {
				background:#b5cfd2;
				border-width: 1px;
				padding: 8px;
				border-style: solid;
				border-color: #999999;
			}
			table td {
				background:#dcddc0;
				border-width: 1px;
				padding: 8px;
				border-style: solid;
				border-color: #999999;
		</style>
	</head>
	<body>
		<h5>Последние 50 платежей (список отсортирован по дате и времени совершения платежа по убыванию)</h5>
		<div style="overflow-x:auto">
			<table border="1">
				<thead>
				<tr>
					<th>id</th>
					<th>external_order_id</th>
					<th>code</th>
					<th>create_on</th>
					<th>account</th>
					<th>pg_salt</th>
					<th>pg_order_id</th>
					<th>pg_payment_id</th>
					<th>pg_amount</th>
					<th>pg_currency</th>
					<th>pg_net_amount</th>
					<th>pg_ps_amount</th>
					<th>pg_ps_full_amount</th>
					<th>pg_ps_currency</th>
					<th>pg_payment_system</th>
					<th>pg_result</th>
					<th>pg_payment_date</th>
					<th>pg_can_reject</th>
					<th>pg_user_phone</th>
					<th>pg_need_phone_notification</th>
					<th>pg_card_brand</th>
					<th>pg_card_pan</th>
					<th>pg_card_hash</th>
					<th>pg_captured</th>
					<th>pg_sig</th>
				</tr>
				</thead>
				<tbody>
					<?php
						$sql = 'SELECT * FROM orders ORDER BY ID DESC LIMIT 50';

						foreach ($pdo->query($sql) as $row) {
						echo '<tr>';
							echo '<td>'.$row["id"].'</td>';
							echo '<td>'.$row["external_order_id"].'</td>';
							echo '<td>'.$row["code"].'</td>';
							echo '<td>'.$row["create_on"].'</td>';
							echo '<td>'.$row["account"].'</td>';
							echo '<td>'.$row["pg_salt"].'</td>';
							echo '<td>'.$row["pg_order_id"].'</td>';
							echo '<td>'.$row["pg_payment_id"].'</td>';
							echo '<td>'.$row["pg_amount"].'</td>';
							echo '<td>'.$row["pg_currency"].'</td>';
							echo '<td>'.$row["pg_net_amount"].'</td>';
							echo '<td>'.$row["pg_ps_amount"].'</td>';
							echo '<td>'.$row["pg_ps_full_amount"].'</td>';
							echo '<td>'.$row["pg_ps_currency"].'</td>';
							echo '<td>'.$row["pg_payment_system"].'</td>';
							echo '<td>'.$row["pg_result"].'</td>';
							echo '<td>'.$row["pg_payment_date"].'</td>';
							echo '<td>'.$row["pg_can_reject"].'</td>';
							echo '<td>'.$row["pg_user_phone"].'</td>';
							echo '<td>'.$row["pg_need_phone_notification"].'</td>';
							echo '<td>'.$row["pg_card_brand"].'</td>';
							echo '<td>'.$row["pg_card_pan"].'</td>';
							echo '<td>'.$row["pg_card_hash"].'</td>';
							echo '<td>'.$row["pg_captured"].'</td>';
							echo '<td>'.$row["pg_sig"].'</td>';
						echo '</tr>';             
						}
						Database::disconnect();
					?>
				</tbody>
			</table>
		</div>
	</body>
</html>