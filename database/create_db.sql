DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_order_id` varchar(255) NULL,
  `code` varchar(255) NULL,
  `create_on` datetime NULL,
  `account` varchar(255) NULL,

  `pg_salt` varchar(255) NULL,
  `pg_order_id` varchar(255) NULL,
  `pg_payment_id` varchar(255) NULL,
  `pg_amount` varchar(255) NULL,
  `pg_currency` varchar(255) NULL,
  `pg_net_amount` varchar(255) NULL,
  `pg_ps_amount` varchar(255) NULL,
  `pg_ps_full_amount` varchar(255) NULL,
  `pg_ps_currency` varchar(255) NULL,
  `pg_payment_system` varchar(255) NULL,
  `pg_result` varchar(255) NULL,
  `pg_payment_date` varchar(255) NULL,
  `pg_can_reject` varchar(255) NULL,
  `pg_user_phone` varchar(255) NULL,
  `pg_need_phone_notification` varchar(255) NULL,
  `pg_card_brand` varchar(255) NULL,
  `pg_card_pan` varchar(255) NULL,
  `pg_card_hash` varchar(255) NULL,
  `pg_captured` varchar(255) NULL,
  `pg_sig` varchar(255) NULL,
  
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;