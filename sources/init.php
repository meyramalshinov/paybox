<?php
include("PG_Signature.php");
include_once("database.php");

$MERCHANT_ID = 9642;
$MERCHANT_SECRET_KEY = "cobitewodolejaxy";

if(!empty($_GET['amount']) && !empty($_GET['currency']) && !empty($_GET['account'])){

	$amount =  $_REQUEST['amount'];
	$currency = $_REQUEST['currency'];
	$account = $_REQUEST['account'];
	//$external_order_id = $_REQUEST['external_order_id'];

	// $amount = 1000;
	// $currency = 'RUR';
	// $account = 'buyer';
	$external_order_id = 'sGqYemvcghnT9jOpJXedt0';

	$arrReq = array();

	/* Обязательные параметры */
	$arrReq['pg_merchant_id'] = $MERCHANT_ID;// Идентификатор магазина
	$arrReq['pg_amount']      = $amount;		// Сумма заказа
	$arrReq['pg_lifetime']    = 3600*24;	// Время жизни счёта (в секундах)
	$arrReq['pg_description'] = "Оплата покупателем: ".$account; // Описание заказа (показывается в Платёжной системе)

	$arrReq['pg_check_url'] = "https://datadiscovery.kz/paybox/check.php"; 
	$arrReq['pg_result_url'] = "https://datadiscovery.kz/paybox/result.php"; 

	$arrReq['pg_success_url'] = 'https://datadiscovery.kz/paybox/payment_ok.php';
	//$arrReq['pg_success_url_method'] = 'AUTOGET';
	$arrReq['pg_failure_url'] = 'https://datadiscovery.kz/paybox/payment_failure.php';

	$salt = rand(21,43433);

	$arrReq['pg_salt'] = $salt;

	$pdo = Database::connect();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$sql = 'INSERT INTO `orders` (external_order_id, create_on, pg_amount, pg_salt, account) VALUES (?, now(), ?, ?, ?)';
	$q = $pdo->prepare($sql);
	$q->execute(array($external_order_id, $amount, $salt, $account));

	$order_id = $pdo->lastInsertId();

	$arrReq['pg_order_id'] = $order_id;		// Идентификатор заказа в системе магазина

	Database::disconnect();

	$arrReq['pg_sig'] = PG_Signature::make('payment.php', $arrReq, $MERCHANT_SECRET_KEY);
	$query = http_build_query($arrReq);
	header("Location: https://www.paybox.kz/payment.php?$query");
}

?>
